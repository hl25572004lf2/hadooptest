﻿using HadoopTest.Service.Interface;
using MapRDB.Driver;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HadoopTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IHadoopService _hadoopService;

        public TestController(IHadoopService hadoopService)
        {
            _hadoopService = hadoopService;
        }

        // GET: api/<TestController>
        [HttpGet]
        public string Get(string domain, string user, string pwd)
        {
            try
            {
                var connection = _hadoopService.GetConnection(domain, user, pwd);

                return "連線成功";
            }
            catch(Exception ex)
            {
                return "連線失敗";
            }

        }

        // GET api/<TestController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {            
            return "value";
        }

        // POST api/<TestController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<TestController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TestController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
