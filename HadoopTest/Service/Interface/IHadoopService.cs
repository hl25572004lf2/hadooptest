﻿using MapRDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HadoopTest.Service.Interface
{
    public interface IHadoopService
    {
        IConnection GetConnection(string domain, string user, string pwd);
    }
}
