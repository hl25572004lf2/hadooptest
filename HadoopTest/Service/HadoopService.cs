﻿using HadoopTest.Service.Interface;
using MapRDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HadoopTest.Service
{
    public class HadoopService : IHadoopService
    {
        public IConnection GetConnection(string domain, string user, string pwd)
        {
            var connectionStr = $"{domain}?auth=basic;" +
            $"user={user};" +
            $"password={pwd};" +
            $"ssl=false;" + 
            $"sslCA=/opt/mapr/conf/ssl_truststore.pem;" +
            $"sslTargetNameOverride=node1.mapr.com";
            return ConnectionFactory.CreateConnection(connectionStr, 3, 5);
        }
    }
}
